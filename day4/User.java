package day4;

public class User {

    private final String firstName;
    private final String lastName;
    private final String company;
    private final String city;
    private final String state;
    private final String county;
    private final int zipCode;
    
    public User(final String firstName, final String lastName, final String company, final String city, final String state, final String county,
            final int zipCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.company = company;
        this.city = city;
        this.state = state;
        this.county = county;
        this.zipCode = zipCode;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getCompany() {
        return company;
    }
    public String getCity() {
        return city;
    }
    public String getState() {
        return state;
    }
    public String getCounty() {
        return county;
    }
    public int getZipCode() {
        return zipCode;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (city == null) {
            if (other.city != null)
                return false;
        } else if (!city.equals(other.city))
            return false;
        if (firstName == null) {
            if (other.firstName != null)
                return false;
        } else if (!firstName.equals(other.firstName))
            return false;
        if (lastName == null) {
            if (other.lastName != null)
                return false;
        } else if (!lastName.equals(other.lastName))
            return false;
        return true;
    }
    @Override
    public String toString() {
        return "User [city=" + city + ", company=" + company + ", county=" + county + ", firstName=" + firstName
                + ", lastName=" + lastName + ", state=" + state + ", zipCode=" + zipCode + "]";
    }

    
    
}
