package day4;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.lang.System.out;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.toSet;

import java.io.*;

public class GroupingByDemo {

    private Set<User> users;

    public GroupingByDemo(){
        this.users = DataInjest.populateUsers();
    }

    public static void main(String[] args) {
        
        GroupingByDemo obj = new GroupingByDemo();
        
        Function<User, String> fetchStateByUser = User :: getState;
        Function<User, String> fetchCityByUser =User :: getCity;

        Map<String, Long> groupedByState = obj.users
            .stream()
            //.collect(Collectors.groupingBy((user) -> user.getState() , Collectors.toSet()));
            //.collect(Collectors.groupingBy(User :: getState , toSet()));
            .collect(Collectors.groupingBy(fetchCityByUser, counting()));

       /*  Set<Map.Entry<String, Set<User>>> entries = groupedByState.entrySet();    

        Iterator<Map.Entry<String, Set<User>>> it = entries.iterator();

        while(it.hasNext()){
            Map.Entry<String, Set<User>> entry = it.next();
            System.out.println("State :: "+ entry.getKey() + " Number of users :: "+ entry.getValue().size());
        } */

        groupedByState
            .keySet()
            .stream()
            .forEach(key -> System.out.println(" Key " + key + " Value " + groupedByState.get(key)));

    }
    /* 
    public static Set<User> loadUsers(){
        return DataInjest.populateUsers();
    } */
}