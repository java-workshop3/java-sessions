package day4.concurrency;

import java.time.LocalDate;

public class ThreadDemo {

    public static void main(String[] args) throws InterruptedException{
        long startTime = System.currentTimeMillis();
        System.out.println(" INside the main method :: " );
        System.out.println(" Name of the main thread is "+ Thread.currentThread().getName());
       /*  DBTask task1 = new DBTask();
        DBTask task2 = new DBTask();
        DBTask task3 = new DBTask();
        DBTask task4 = new DBTask(); */

        Thread t1 = new Thread(() -> connectToDB());
        Thread t2 = new Thread(() -> connectToDB());
        Thread t3 = new Thread(() -> connectToDB());
        Thread t4 = new Thread(() -> connectToDB());

        t1.start();
        t2.start();
        t3.start();
        t4.start();

        System.out.println("This statement is released now ...");
        //Thread.sleep(10_000);
        t1.join();
        t2.join();
        t3.join();
        t4.join();
        System.out.println(" Completed the main method :: " + (System.currentTimeMillis() - startTime)/1000);
    }

    private static void connectToDB(){
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(" THe DB task is completed ....");
    }
}

class DBTask implements Runnable {

    @Override
    public void run() {
     connectToDB();   
    }
    private static void connectToDB(){
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(" THe DB task is completed ....");
    }
}
