package day4.concurrency;

import java.util.stream.IntStream;

public class Printer {
    public void print(int pages, String customerName){
        IntStream.range(1, pages+1)
                .forEach((index) -> {
                    try {
                        Thread.sleep(2000);
                        System.out.println( "printing "+ index + " of Customer "+ customerName);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
    }
}
