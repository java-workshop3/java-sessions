package day4.concurrency;

import static java.util.concurrent.Executors.newFixedThreadPool;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.stream.IntStream.range;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.*;
import java.util.function.*;


public class ExecutorsDemo {
    public static void main(String[] args) throws InterruptedException {

        //Function<String, Integer> fn = Integer::parseInt;
        //System.out.println(fn.apply("45"));
        // BiFunction<Integer, Integer, String> add = (operand1, operand2) -> "The sum of two numbers ara "  + (operand1 + operand2);
        // System.out.println(add.apply(34, 56));

        Predicate<Integer> isEven = (num) -> num % 2 == 0;
        Predicate<Integer> isOdd = isEven.negate();

        range(1, 200)
            .forEach(index -> {
                int random = (int)Math.ceil(Math.random()* 4555);
                System.out.println("Is even :: "+random + " >> " + isEven.test(random));
                System.out.println("Is even :: "+random + " >> " + isOdd.test(random));
            });
        

        Printer printer = new Printer();

        Task task1 = new Task(printer, "John", 4);
        Task task2 = new Task(printer, "Harish", 3);
        Task task3 = new Task(printer, "Seema", 6);
        Task task4 = new Task(printer, "Vikram", 5);
        //List<Task> tasks = Arrays.asList(task1, task2, task3, task4);
        ExecutorService executorService =  Executors.newCachedThreadPool();
        /* tasks.forEach(task -> {
            executorService.submit(task);
        }); */

      /*   Arrays.asList(task1, task2, task3, task4)
        .forEach(executorService::submit);
        
        executorService.shutdown();
        executorService.awaitTermination(1, MINUTES);
        System.out.println("Completed the execution of the tasks"); */
    }
}
