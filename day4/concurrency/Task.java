package day4.concurrency;

public class Task implements Runnable{

    private final Printer printer;
    private final String customerName;
    private final int numberOfPages;

    public Task(Printer printer, String customerName, int numberOfPages){
        this.printer = printer;
        this.customerName = customerName;
        this.numberOfPages = numberOfPages;
    }
    @Override
    public void run() {
        synchronized(printer){
            this.printer.print(this.numberOfPages, customerName);
        }
    }
}
