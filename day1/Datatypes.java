package day1;

public class Datatypes {

    public static void main(String[] args) {
        
        byte b = (byte)128;

        //upcasting is free from the compiler
        int intVal = b;

        int val = 290;

        //downcasting - should be explicitly done
        // if the value is smaller, works normally
        //in case the value does not fit the container, the modulo operator will be applied and the data will be shrinked
        // no error but can lead to bug in the application.
        byte bVal = (byte)val;

        System.out.println("Byte value is :: "+ bVal);
    }
    
}
