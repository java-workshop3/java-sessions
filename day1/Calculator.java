package day1;

public class Calculator {

    public int sum(int a, int b){
        return a + b;
    }
    
    public int sub(int a, int b){
        return a - b;
    }
    public int product(int a, int b){
        return a * b;
    }
    public int divide(int a, int b){
        return a / b;
    }
    public int modulo(int a, int b){
        return a % b;
    }
}
