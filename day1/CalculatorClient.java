package day1;

public class CalculatorClient {

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        int result = calculator.sum(34, 56);
        System.out.printf("Sum of two numbers is %d", result);
        
        int subResult = calculator.sum(340, 56);
        System.out.printf("Difference of two numbers is %d", subResult);

        int productResult = calculator.product(340, 56);
        System.out.printf("Product of two numbers is %d", productResult);
    }
    
}
