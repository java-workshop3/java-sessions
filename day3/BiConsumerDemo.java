package day3;

import java.util.function.BiConsumer;
import java.util.function.IntSupplier;

public class BiConsumerDemo {
    public static void main(String[] args) {
        BiConsumer<Integer, Integer> printTwoIntegers = (a, b) -> saveToDB(a , b);
        //printTwoIntegers.accept(34, 45);
        IntSupplier suppier = ( ) -> (int)(Math.random()* 1000);
        for( int index = 0; index < 10; index ++){
            System.out.println(suppier.getAsInt());
        }
       //System.out.println(IntStream.range(10, 455).sum());
    }
    
    public static int saveToDB(int a, int b){
     //connect to db;
      System.out.println("saving the data to the database " + a + " b "+ b);
      return 34;
    }
}