package day3;

import java.util.*;
import java.io.*;

public class DataInjest {

    private static final String fileLocation = "D:\\data\\address.csv";
    
    public static Set<User> populateUsers(){
        Set<User> users = new HashSet<>();
        try (
                FileReader    fileReader = new FileReader(fileLocation);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
            ) {
            boolean flag = true;
            while(flag){
                String content =  bufferedReader.readLine();
                if (content == null){
                 flag = false;
                 continue;   
                }
                String [] data = content.split(",");
                String firstName = data[0];
                String lastName = data[1];
                String company = data[2];
                String city = data[4];
                String county = data[5];
                String state = data[6];
                String zipCode = data[7];
                
                User user = new User(firstName, lastName, company, city, state, county, zipCode);
                users.add(user);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File with the given name is not present");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Unable to read from the file");
            e.printStackTrace();
        } /*
           // Implementing the try with resources will help to automatically clean up the resources 
           // and avoid memory leaks if not closed correctly by the developers. 
            finally {
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            if (bufferedReader != null ){
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        } */
        return users;
    }
}

