package day3;

import java.util.*;

public class UsersCrudAPI {

    private Set<User> users;

    public UsersCrudAPI(){
         users = DataInjest.populateUsers();
    }

    public Set<User> fetchAllUsers(){
        return this.users;
    }

    public User findUserByFirstName(String firstName){
        /* Optional<User> optionalUser = this.users
                                            .stream()
                                            .filter(user -> user.getFirstName().equals(firstName))
                                            .findFirst();
        if(optionalUser.isPresent()){
            return optionalUser.get();
        }                             
        throw new IllegalArgumentException("User with the given first name does not exists");  */              

        return this.users
                    .stream()
                    .filter(user -> user.getFirstName().equals(firstName))
                    .findFirst()
                    .orElseGet(() -> new User("Rsyus", "Kumar", "Capgemini" , "bangalore" , "karnataka", "mysore", "577474"));
                    //.orElseThrow(() -> new IllegalArgumentException("No such user present"));
    }

    public boolean deleteUserByFirstName(String firstName){
        return this.users.removeIf(user -> user.getFirstName().equals(firstName));
    }
}
