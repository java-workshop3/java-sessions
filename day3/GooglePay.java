package day3;
public class GooglePay implements PaymentGateway, PhoneRecharge{

    private static int counter = 0;
    @Override
    public void pay(String from, String to, double amount, String notes) throws PaymentGatewayException {
        counter++;
        if ( counter % 3 != 0) {
            System.out.printf("Paying Rs %s from %s to %s using Google Pay %n", amount, from, to);        
            System.out.println(" You have earned a reward point of Rs: "+ 0.01 * amount);
        } else {
            throw new PaymentGatewayException("Exception while making the payment:: ");
        }
    }

    @Override
    public void recharge(String phoneNumber, double amount) {
        System.out.printf("Successfully recharged Phone number: %s with  Rs %s  using Google Pay %n", phoneNumber, amount);        
        System.out.println(" You have earned a reward point of Rs: "+ 0.01 * amount);
    }

}
