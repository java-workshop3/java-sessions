package day3;

public class PaymentGatewayProviders  {
    
    static PaymentGateway gateway = (from,to,amount,notes) -> System.out.printf("Paying Rs %s from %s to %s using Cash on Delivery %n", amount, from, to);        
        
    
    public static void main(String[] args) throws PaymentGatewayException{
        gateway.pay("Ramesh", "Suresh", 10000, "test");
    }
}
