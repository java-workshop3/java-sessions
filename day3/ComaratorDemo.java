package day3;

import static day3.Comparators.addressZipCodeDescComparator;

import java.util.Iterator;
import java.util.TreeSet;
import java.util.Set;

public class ComaratorDemo {
    public static void main(String[] args) {
        Address address1 = new Address("Bangalore", "Karnatana", 22222);
        Address address2 = new Address("Chennai", "TamilNadu", 33333);
        Address address3 = new Address("Hyderabad", "Telangana", 44444);
        Address address4 = new Address("Mumbai", "Maharastra", 555555);

        Set<Address> addressSet = new TreeSet<Address>(addressZipCodeDescComparator);
        addressSet.add(address1);
        addressSet.add(address2);
        addressSet.add(address3);
        addressSet.add(address4);
        Iterator<Address> it = addressSet.iterator();
        while(it.hasNext()){
            System.out.println(it.next());
        }

    }
}
