package day3;

import java.util.Scanner;

public class PaymentClient {
    public static void main(String[] args) {// throws PaymentGatewayException

        
        System.out.println("Choose your preferred platform for payment");
        System.out.println("1 -> Google Pay");
        System.out.println("2 -> Phone Pay");

        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        PaymentGateway gateway = null;
        PhoneRecharge app = null;

        if (option == 1){
            gateway = new GooglePay();
            app = new GooglePay();
        } else {
            gateway = new PhonePay();
            app = new PhonePay();
        }
        try{
            gateway.pay("Vindod", "Kishore", 15_000, "towards home loan");
            gateway.pay("Vindod", "Kishore", 25_000, "towards home loan");
            gateway.pay("Vindod", "Kishore", 35_000, "towards home loan");
            gateway.pay("Vindod", "Kishore", 45_000, "towards home loan");
         } catch(PaymentGatewayException e) {
             System.out.println( " There was an exception during the payment "+ e.getMessage());
             throw new IllegalArgumentException("System error: Please try after some time");
         }
        //app.recharge("9655484545", 250);
        scanner.close();

    }
}
