package day3;

public class PaymentGatewayException extends Exception {

    public PaymentGatewayException(String message){
        super(message);
    }

    @Override 
    public String getMessage(){
        return super.getMessage();
    }
}
