package day2;
import java.util.ArrayList;
import java.util.List;

public abstract class Employee {

    private static long counter = 1000;
    private String name;
    private String emailAddress;
    private final long empId;
    private List<Address> addresses = new ArrayList<>();

    public Employee(String name, String emailAddress){
        this.empId = counter++;
        this.name = name;
        this.emailAddress = emailAddress;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public long getEmpId(){
        return this.empId;
    }

    public void setEmailAddress(String emailAddress){
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress(){
        return this.emailAddress;
    }
    
    public void addAddress(Address address){
        this.addresses.add(address);
    }

    public List<Address> getAddresses(){
        return this.addresses;
    }

    public abstract void applyForLeave(int noOfDays) throws InsufficientLeaveBalanceException;

    public abstract double calculateTDS();
}
