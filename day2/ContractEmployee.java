package day2;

public class ContractEmployee extends Employee {

    private int duration;

    private double payPerHour;

    public ContractEmployee(String name, String emailAddress){
        super(name, emailAddress);
    }
    public ContractEmployee(String name, String emailAddress, int duration, double payPerHour){
        super(name, emailAddress);
        this.duration = duration;
        this.payPerHour = payPerHour;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public double getPayPerHour() {
        return payPerHour;
    }

    public void setPayPerHour(double payPerHour) {
        this.payPerHour = payPerHour;
    }

    @Override  
    public void applyForLeave(int noOfDays) throws InsufficientLeaveBalanceException{
        if(noOfDays < 4 && this.duration > noOfDays){
            this.duration = this.duration - noOfDays;
        } else {
            throw new InsufficientLeaveBalanceException("Cannot exceed more than 4 leaves");
        }
    }

    @Override
    public double calculateTDS(){
        return (this.payPerHour * duration * 8 * 10/100);
    }
}
