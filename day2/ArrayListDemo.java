package day2;

import java.util.*;

//https://docs.oracle.com/javase/8/docs/api/index.html?java/util/ArrayList.html
public class ArrayListDemo {

    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        names.add("Kiran");
        names.add("Vishnu");
        names.add("Mahira");
        names.add(1, "Hari");

        //1. Using traditional for loop
        for (int index = 0; index < names.size(); index++) {
            names.get(index);
        }

        //2. iterator
        names.iterator();
        //3. for each
        for(String name: names){
           System.out.println(name);  
        }

        //Applicable only for List
        ListIterator<String> lIt = names.listIterator();
        
        //4. functional style
        names.forEach(name -> System.out.println(name));
        System.out.println("Name at 0th position is "+names.get(0));
        System.out.println("Is Kiran present " +names.contains("Kiran"));
        System.out.println("Is names empty " +names.isEmpty());
        names.clear();
        System.out.println("Is names empty after calling the clear metho:: " +names.isEmpty());
    }
}
