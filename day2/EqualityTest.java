package day2;

public class EqualityTest {
    public static void main(String[] args) {
        Address address1 = new Address("Bangalore", "Karnataka", 577142);
        Address address2 = address1;

        Address address3 = new Address("Bangalore", "Karnataka", 577142);

        System.out.println("Are the two addresses equal? " + (address1 == address2));

        System.out.println("Are the two addresses equals by content ? "+ (address1.equals(address3)));
    }
}
