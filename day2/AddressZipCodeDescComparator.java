package day2;

import java.util.Comparator;

public class AddressZipCodeDescComparator implements Comparator<Address>{

    public int compare(Address address1, Address address2){
        return address2.getZipCode() - address1.getZipCode();
    }
    
}
