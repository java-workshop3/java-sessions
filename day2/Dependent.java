package day2;

public class Dependent {
    private String name;
    private int age;
    private Relationship relationship;
    private Gender gender;

    public Dependent(String name, int age, Relationship relationship, Gender gender) {
        this.name = name;
        this.age = age;
        this.relationship = relationship;
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public Relationship getRelationship() {
        return relationship;
    }


@Override
    public String toString() {
        return "Dependent [age=" + age + ", gender=" + gender + ", name=" + name + ", relationship=" + relationship
                + "]";
    }
}

enum Relationship {
    SON,
    DAUGHTER,
    FATHER,
    MOTHER,
    SPOUSE
}
enum Gender {
    MALE,
    FEMALE
}
